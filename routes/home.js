// we call express library  
var express = require('express');

// define router as express.Router()
var router = express.Router();

// call check login session middleware
var check_login_session = require('../middleware/check_login_session')

// load employee models
var EmployeeModel = require('../models/employee');

// CSRF Protection
var csrf = require('csurf')
var csrfProtection = csrf({ cookie: true })

// -----------------------------------------------------//

// this mean all route and all method will check the login session
// give the route.all with check login session middleware
router.all('*', check_login_session, (req, res, next) => {
  next();
})

// redirect to /index
router.get('/', function (req, res, next) {
  res.redirect('/home/index');
});

router.get('/index', (req, res, next) => {
  // catch data from session
  var {
    username
  } = req.session;

  // render home.ejs and pass variable from route to view
  res.render('home', {
    // pass the data to view
    username: username,
    data1: '1', //etc   
  })
})

// URL for add data employee
router.get('/employee', csrfProtection, (req, res, next) => {
  // catch data from session
  var {
    username
  } = req.session;

  // render add_employee.ejs
  res.render('add_employee', {
    // pass the data to view
    username: username,
    csrfToken: req.csrfToken()
  })
})

// insert process
router.post('/insert_employee', csrfProtection, async (req, res, next) => {
  // catch data from <form name=>
  var {
    nik,
    nama,
    divisi,
    department,
    posisi,
    cabang,
    jk
  } = req.body;

  // insert process
  var cek_data = await EmployeeModel.findOne({
    where: {
      nik: nik
    }
  })

  // Check duplicate data
  // If found duplicate data, user will redirect back to form and show flash message
  if (cek_data != null) {
    // set flash message
    req.flash('info', '<div class="alert alert-info text-center">Nik sudah ada</div>');
    // redirect to form
    res.redirect('/home/employee')
  } else {
    var insert_data = await EmployeeModel.create({
      nik: nik,
      nama: nama,
      jenis_kelamin: jk,
      divisi: divisi,
      department: department,
      posisi: posisi,
      cabang: cabang
    })

    if (insert_data) {
      // set flash message
      req.flash('info', '<div class="alert alert-success text-center">Insert Success</div>');
      /// redirect to form
      res.redirect('/home/employee')
    } else {
      // set flash message
      req.flash('info', '<div class="alert alert-danger text-center">Insert Fail</div>');
      // redirect to form
      res.redirect('/home/employee')
    }


  }

})

// Route to show data
router.get('/data', async (req, res, next) => {
  // catch data from session
  var {
    username
  } = req.session;

  // get all data
  var data = await EmployeeModel.findAll();

  // render list_data.ejs
  res.render('list_data', {
    // pass the data to view
    username: username,
    data: data
  })
})

// Route for delete data
router.get('/delete_employee/:nik', async (req, res, next) => {
  // catch data from url param
  // ex url : localhost:3000/delete_employee/0441380718
  // this will give value '0441380718' to nik variable
  var {
    nik
  } = req.params;

  // Delete data
  var query = await EmployeeModel.destroy({
    where: {
      nik: nik
    }
  })

  if (query) {
    // set flash message
    req.flash('info', '<div class="alert alert-success text-center">Delete Success</div>');
    /// redirect to form
    res.redirect('/home/data')
  } else {
    // set flash message
    req.flash('info', '<div class="alert alert-danger text-center">Delete Fail</div>');
    // redirect to form
    res.redirect('/home/data')
  }
})

router.get('/about', (req, res, next) => {
  // catch data from session
  var {
    username
  } = req.session;

  // Render about.ejs
  res.render('about', {
    // pass the data to view
    username: username
  })
})

// Route to show edit form
router.get('/edit_employee/:nik', csrfProtection, async (req, res, next) => {
  // catch data from session
  var {username} = req.session;

  // catch data from url param
  var {nik} = req.params;

  var data = await EmployeeModel.findOne({
    where: {
      nik: nik
    }
  })

  if (data != null) {
      // render views edit_data.ejs and pass the data
      res.render('edit_data', {
        username: username,
        data: data,
        csrfToken: req.csrfToken()
      })
  } else {
      // set flash message
      req.flash('info', '<div class="alert alert-info text-center">Data not Found</div>');
      /// redirect to form
      res.redirect('/home/data')
  }

})

// Route for handle edit process
router.post('/edit_employee', csrfProtection, async (req, res, next) => {
  // catch data post from <form>
  var {
    nik,
    nama,
    divisi,
    department,
    posisi,
    cabang,
    jk
  } = req.body;

  // Update query
  var query = await EmployeeModel.update({
    nama:  nama,
    divisi: divisi,
    jenis_kelamin: jk,
    department: department,
    posisi: posisi,
    cabang: cabang
    }, {
      where:  {
        nik: nik
      },
            returning: true,
            plain: true
    }
  
  )
  
  if(query) {
     // set flash message
     req.flash('info', '<div class="alert alert-success text-center">Edit Success</div>');
     /// redirect to form
     res.redirect('/home/data')
  } else {
     // set flash message
     req.flash('info', '<div class="alert alert-danger text-center">Edit Fail</div>');
     /// redirect to form
     res.redirect('/home/data')
  }

})


module.exports = router;