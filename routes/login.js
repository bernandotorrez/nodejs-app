// we call express library  
var express = require('express'); 

// define router as express.Router()
var router = express.Router();     

// Load md5, because i use md5 to encrypt the password
var md5 = require('md5');

// load Login Model in models folder
var LoginModel = require('../models/login');

// Load Constants variable
var constant = require('../config/constants');

// --------------- LDAP Auth --------------- //
// Load LDAP Library
var ActiveDirectory = require('activedirectory');
// Ldap Configuration
var config = {
  url: 'ldap://ldap.bussan.co.id',
  baseDN: 'DC=bussan,DC=co,DC=id',
  username: 'CN=eyasrvc,OU=Services,OU=Information Technology,OU=Headquarter,DC=bussan,DC=co,DC=id',
  password: 'Bussan100'
}
var ad = new ActiveDirectory(config);
// --------------- LDAP Auth --------------- //

// CSRF Protection
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true })

// Load SOAP Library for WebService
var soap = require('soap');


// -----------------------------------------------------//

// this mean... as route index
router.get('/', csrfProtection, function(req, res, next) {  
  res.render('login', {
    csrfToken: req.csrfToken()
  });
});

// see? i use .post in route, that mean
// this route handle the post method / request
router.post('/do_login', csrfProtection, async (req, res, next) => { 
 
  // to catch data in method post, we should use req.body
  var {username, password} = req.body;
  
  // this ORM query is using (sequelize) library, and equal to :
  // select * from login where username = ? and password = ? limit 1
  var query = await LoginModel.findOne({
    where: {
      username: username,
      password: md5(password)
    }
  })

  if(query) {
    // set session
    req.session.username = query.username

    // redirect to url home
    res.redirect('/home')
  }
  else {
    // set flash message
    req.flash('info', '<div class="alert alert-danger text-center"><strong>Username</strong> atau <strong>Password</strong> anda Salah!</div>');
    // redirect the url to login
    res.redirect('/login')  
  } 
})

// Route to handle log out
router.get('/logout', (req, res, next) => {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }

})

// Route Login with LDAP
router.get('/ldap', (req, res, next) => {
  // render login_ldap.ejs
  // then open /login/ldap in url
  res.render('login_ldap');
})

// Route to handle LDAP Auth
router.post('/ldap', (req, res, next) => {
  // catch 'post' data from <form>
  var {
    nik,
    password
  } = req.body;

  // Check ldap data by nik
  adFindUser(nik, (result) => {

    let cn = `CN=${result.cn}`;   // get employee domain
    // check domain and password to ldap
    adAuthenticate(result.dn, password, (login) => {

      // if domain and password is found in ldap
      if (login === true) {
        adFindUsers(cn, (results) => {
          var {
            givenName,
            employeeID
          } = results

          req.session.username = givenName 

          res.redirect('/home')
        })
      } else {
        req.flash('info', '<div class="alert alert-danger text-center"><strong>NIK</strong> atau <strong>Password</strong> anda Salah!</div>');
        res.redirect('/login/ldap');
      }

    })
  })
})

// Web Service Example
router.get('/ws/:nik', (req, res, next) => {
  // Catch data from url params, ex : /ws/0441380718
  var {nik} = req.params;
  WSGetDataEmployeeByNIK(nik, (result) => {
    res.send(result)
  })
})

// Function Active Directory
function adFindUser(nik, callback) {
  let msg, status;
  // ad.findUser is a activedirectory function
  ad.findUser(nik, function (err, user) {
    
    // callback is same to return (return the data)
    if (err) {
      callback(err)
      return;
    }

    if (!user) {

      callback({
        status: 'gagal',
        msg: 'User: ' + nik + ' not found.'
      });
    } else {
      callback(user)
    }
  });
}

function adFindUsers(cn, callback) {
  ad.findUsers(cn, true, function (err, results) {
    if ((err) || (!results)) {
      callback('ERROR: ' + JSON.stringify(err));
      return;
    }

    callback(results[0])

  });
}

function adAuthenticate(dn, password, callback) {
  // check ldap authentication based on your nik and password
  ad.authenticate(dn, password, function (err, auth) {
    if (err) {
      callback(err.description);
      return;
    }

    if (auth) {
      callback(auth)

    } else {
      callback('Login Gagal!');
    }

  });
}
// Function Active Directory

// Function Web Service
function WSGetDataEmployeeByNIK(no_karyawan, callback) {
  // Get URL WSDL Web Service from constants.js in config folder
  const url = constant.url_ws_employee;

  // Because Web Service that i'll use is need a parameter
  // i'll pass the parameter docGetEmployeeDataDynRequest and NO_KARYAWAN
  // this is the request of the webservice , example :

  // <docGetEmployeeDataDynRequest>             parameter 1 
  //   <NO_KARYAWAN>0441380718</NO_KARYAWAN>    parameter 2
  // </docGetEmployeeDataDynRequest>

  let arg = {
    docGetEmployeeDataDynRequest: {             // parameter 1
      NO_KARYAWAN: no_karyawan                  // parameter 2
    }
  }

  soap.createClientAsync(url).then((client) => {
    
    client.getEmployeeData1Dyn(arg, function (err, result, rawResponse, soapHeader, rawRequest) {


      if (err) {
        callback(err)
        return
      } else {
        if (!result) {
          callback('Data tidak Ditemukan')

        } else {
          var data = result
        }
        callback(data)
      }
    })
  })
}
// Function Web Service

module.exports = router;    
