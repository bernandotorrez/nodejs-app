if(process.env.NODE_ENV=='development'){
  var config = {
    user: 'er-tracking',
    password: 'NetApp',
    server: '172.16.1.187',
    database: 'nodejs-app' 
  };
} else {
  var config = {
    user: 'er-tracking',
    password: 'NetApp',
    server: '172.16.1.187',
    database: 'nodejs-app' 
  };
}

const Sequelize = require('sequelize');

const sequelize = new Sequelize(config.database, config.user, config.password, {
    host: config.server,
    dialect: 'mysql',
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  });

module.exports = sequelize;