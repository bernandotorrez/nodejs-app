module.exports = function (req, res, next) { 
    // 401 Unauthorized
    // 403 Forbidden 

    if (!req.session.username || req.session.username == '') {    
    //if session if null, send http status 401 
    // and text 'you are not logged in'
            return res.status(401).send('you are not logged in');
    }
    
   next()
}